# Project CLI

How to use:

1. Clone this repo in a directory.
2. Clone all its dependencies to the same directory at the same level (eg.: /app/cli, /app/other-services)
3. Create a file named `.env` with all required credentials (see `docker-compose.yml`)
4. Start everything with `make run` (this will build all the dependencies, then start the apps).
5. You also need to properly setup your Git configuration repository for the `configuration-server`.

Reach out to me if you need help: https://www.twitter.com/vrcca

