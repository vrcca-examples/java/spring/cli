build: build-servers build-services
services: licensing-service organization-service
servers: configuration-server eureka-server zuul-server authentication-service zipkin-server
run: build start

.PHONY : configuration-server licensing-service organization-service eureka-server zuul-server authentication-service zipkin-server

configuration-server:
	+$(MAKE) -C ../$@

licensing-service:
	+$(MAKE) -C ../$@

organization-service:
	+$(MAKE) -C ../$@

eureka-server:
	+$(MAKE) -C ../$@

zuul-server:
	+$(MAKE) -C ../$@

authentication-service:
	+$(MAKE) -C ../$@

zipkin-server:
	+$(MAKE) -C ../$@

stop:
	docker-compose stop

start:
	docker-compose up -d

display-logs:
	docker-compose logs --tail=100 -f
